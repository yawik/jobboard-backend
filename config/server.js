module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
  url: env('STRAPI_PUBLIC_URL', 'https://api.jobsintown.de')
});
